"use client";
import { RiMoonClearFill } from "react-icons/ri";
import {
  AiFillTwitterCircle,
  AiFillLinkedin,
  AiFillGithub,
  AiFillGitlab,
} from "react-icons/ai";
import { useState } from "react";

export default function Home() {
  const [darkMode, setDarkMode] = useState(false);

  const openResume = () => {
    window.open("../static/resume/bhaskar_resume.pdf", "_blank");
  };

  return (
    <div className={darkMode ? "dark" : ""}>
      <main className="bg-white px-10 dark:bg-gray-800">
        <section className="min-h-screen pb-20">
          <title>Portfolio</title>
          <nav className="py-10 mb-12 flex justify-end">
            <ul className="flex items-center">
              <li>
                <RiMoonClearFill
                  onClick={() => setDarkMode(!darkMode)}
                  className="cursor-pointer text-2xl"
                ></RiMoonClearFill>
              </li>
              <li>
                <button
                  onClick={openResume}
                  className="bg-blue-800 text-white px-4 py-3 rounded-lg ml-8"
                >
                  Resume
                </button>
              </li>
            </ul>
          </nav>

          <div className="text-center p-10">
            <h2 className="text-5xl py-2 text-orange-300 font-medium font-helvetica">
              Bhaskar Nhuchhe Pradhan
            </h2>
            <h3 className="text-2xl py-2 font-helvetica font-bold dark:text-white">
              Software Engineer
            </h3>
            <p className="text-2xl py-5 leading-20 text-gray-500 font-helvetica dark:text-white">
              I am a software engineer working on backend development using Java
              and Node for the past 5 years. I am passionate about backend
              technologies and writing clean and scalable code.
            </p>
          </div>

          <div className="text-4xl justify-center space-x-2 flex py-1 text-blue-800">
            <a
              target="_blank"
              rel="noopener noreferrer"
              href="https://www.linkedin.com/in/bhaskar-nhuchhe-pradhan-168ba6148"
            >
              <AiFillLinkedin></AiFillLinkedin>
            </a>

            <a
              target="_blank"
              rel="noopener noreferrer"
              href="https://twitter.com/bhaskr11"
            >
              <AiFillTwitterCircle></AiFillTwitterCircle>
            </a>

            <a
              target="_blank"
              rel="noopener noreferrer"
              href="https://github.com/bhaskr11"
            >
              <AiFillGithub></AiFillGithub>
            </a>

            <a
              target="_blank"
              rel="noopener noreferrer"
              href="https://gitlab.com/bhaskr11"
            >
              <AiFillGitlab></AiFillGitlab>
            </a>
          </div>
        </section>
      </main>
    </div>
  );
}
